# LeanGoWithTests
* Go 1.14で実施
## Hello World:2h ○
* 1.14だとテストコードでエラーになるかも
## Integers:0.75h
* GoDoc出力できない。バージョンのせい？
## Iteration:0.75h ○
## Arrays and slices:1.5h
## Structs, methods & interfaces:1h ○
## Pointers & errors:2.25h 
## Maps：1.5h
## Dependency Injection:45h ○
* JavaのDIとは根本的に違う
* http://locahots:5050 でアクセスできなかったが、一旦無視
## Mocking:2h ○
* **超重要。** TDDの肝。JavaのMockというより、テストしやすいコードをかけ（設計しろ）ということ。
## Select:1h
## Reflection:2h
* むずい
## Context:1h
* むずい
## Intro to property based tests:1.5h
* 長い、わかりずらい（難しくはない）





